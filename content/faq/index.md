---
title: "FAQ"
author: ["Jacob Hilker"]
draft: false
---

Here are some frequently asked questions about the setting. I will keep this updated as best I can.

<details>
<summary>Why did you publish your bibliography and your FAQ pages on the wiki?</summary>
<div class="details">

-   In case anyone wanted to see what I had used for research, or if there were any questions they might have already had.
</div>
</details>
